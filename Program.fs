﻿open Demo
open System.Text.Json

let incomingJson = """{"Lat":51, "Lon":-0.9}"""
printfn "incomingJson \n%A" incomingJson

let unvalidatedPosition = JsonSerializer.Deserialize<UnvalidatedPosition> incomingJson 
printfn "UnvalidatedPosition \n%A" unvalidatedPosition

let position = Position.create unvalidatedPosition
match position with
| Ok p -> printfn "Position \n%A" p
| Error e -> printfn "Error: \n%A" e

// 2 workflows:
// 1 - save the data to db
// 2 - get the data from db