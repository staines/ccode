namespace Demo
open Domain

type UnvalidatedPosition = {
    Lat: decimal
    Lon: decimal
}

module Position = 

    let create unvalidatedPosition = 
        let lat = Latitude.create unvalidatedPosition.Lat
        let lon = Longitude.create unvalidatedPosition.Lon
        match lat, lon with
        | (Ok a, Ok b) -> Ok (Position (a, b))
        | _ -> Error "Unable to create Position from given values"
