namespace Demo

//[<AutoOpen>]
module Domain =

    //[<AutoOpen>]
    type Latitude = private Latitude of decimal
    module Latitude = 
        let create (input: decimal) = 
            if input >= -90m && input <= 90m then
                Ok (Latitude input)
            else
                Error "Latitude should be between -90° and 90°"
    
    type Longitude = private Longitude of decimal
    module Longitude = 
        let create (input: decimal) = 
            if input >= -180m && input <= 180m then
                Ok (Longitude input)
            else
                Error "Longitude should be between -180° and 180°"

    type Position = Latitude * Longitude
